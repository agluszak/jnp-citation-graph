#ifndef JNP_CITATION_GRAPH_CITATIONGRAPH_H
#define JNP_CITATION_GRAPH_CITATIONGRAPH_H

#include <vector>
#include <memory>
#include <unordered_set>
#include <map>
#include <set>

class PublicationAlreadyCreated : public std::exception {
    const char* what() const noexcept override {
        return "PublicationAlreadyCreated";
    }
};

class PublicationNotFound : public std::exception {
    const char* what() const noexcept override {
        return "PublicationNotFound";
    }
};

class TriedToRemoveRoot : public std::exception {
    const char* what() const noexcept override {
        return "TriedToRemoveRoot";
    }
};

template<class Publication>
class CitationGraph {
    struct Node;
    using PublicationId = typename Publication::id_type;
    using Map = std::map<PublicationId, Node*>;
    using MapIterator = typename Map::iterator;

    struct Node : std::enable_shared_from_this<Node> {
        Publication publication;
        std::set<std::shared_ptr<Node>, std::owner_less<std::shared_ptr<Node>>> children;
        std::set<Node*> parents;
        Map* map;
        MapIterator iterator;

        Node(PublicationId const& publication_id, Map* map_ptr) :
                publication{publication_id}, map{map_ptr}, iterator{map_ptr->end()} {}

        ~Node() {
            for (std::shared_ptr<Node> child_ptr: children) {
                child_ptr->parents.erase(this);
            }
            if (iterator != map->end()) {
                map->erase(iterator);
            }
        }
    };

    std::shared_ptr<Map> map;
    std::shared_ptr<Node> root;

public:
    CitationGraph(PublicationId const& stem_id) {
        map = std::make_shared<Map>();
        root = std::make_shared<Node>(stem_id, map.get());
        std::tie(root->iterator, std::ignore) = map->emplace(stem_id, root.get());
    }

    // Konstruktor przenoszący i przenoszący operator przypisania. Powinny być
    // noexcept.
    CitationGraph(CitationGraph<Publication>&& other) noexcept {
        swap(map, other.map);
        swap(root, other.root);
    }

    CitationGraph<Publication>& operator=(CitationGraph<Publication>&& other) noexcept {
        swap(this->map, other.map);
        swap(this->root, other.root);
        return *this;
    }

    // Zwraca identyfikator źródła. Metoda ta powinna być noexcept wtedy i tylko
    // wtedy, gdy metoda Publication::get_id jest noexcept. Zamiast pytajnika należy
    // wpisać stosowne wyrażenie.
    PublicationId get_root_id() const noexcept(noexcept(std::declval<Publication>().get_id())) {
        return root->publication.get_id();
    }

    // Zwraca listę identyfikatorów publikacji cytujących publikację o podanym
    // identyfikatorze. Zgłasza wyjątek PublicationNotFound, jeśli dana publikacja
    // nie istnieje.
    std::vector<PublicationId> get_children(PublicationId const& id) const {
        if (!exists(id)) {
            throw PublicationNotFound();
        }
        std::vector<PublicationId> children_ids;
        for (auto& ptr: map->at(id)->children) {
            children_ids.emplace_back(ptr->publication.get_id());
        }
        return children_ids;
    }

    // Zwraca listę identyfikatorów publikacji cytowanych przez publikację o podanym
    // identyfikatorze. Zgłasza wyjątek PublicationNotFound, jeśli dana publikacja
    // nie istnieje.
    std::vector<PublicationId> get_parents(PublicationId const& id) const {
        if (!exists(id)) {
            throw PublicationNotFound();
        }
        std::vector<PublicationId> parent_ids;
        for (auto& ptr: map->at(id)->parents) {
            parent_ids.emplace_back(ptr->publication.get_id());
        }
        return parent_ids;
    }

    // Sprawdza, czy publikacja o podanym identyfikatorze istnieje.
    bool exists(PublicationId const& id) const {
        return map->find(id) != map->end();
    }

    // Zwraca referencję do obiektu reprezentującego publikację o podanym
    // identyfikatorze. Zgłasza wyjątek PublicationNotFound, jeśli żądana publikacja
    // nie istnieje.
    Publication& operator[](PublicationId const& id) const {
        if (!exists(id)) {
            throw PublicationNotFound();
        }
        return map->at(id)->publication;
    }

    // Tworzy węzeł reprezentujący nową publikację o identyfikatorze id cytującą
    // publikacje o podanym identyfikatorze parent_id lub podanych identyfikatorach
    // parent_ids. Zgłasza wyjątek PublicationAlreadyCreated, jeśli publikacja
    // o identyfikatorze id już istnieje. Zgłasza wyjątek PublicationNotFound, jeśli
    // któryś z wyspecyfikowanych poprzedników nie istnieje.
    void create(PublicationId const& id, PublicationId const& parent_id) {
        create(id, std::vector<PublicationId>{parent_id});
    }

    void create(PublicationId const& id, std::vector<PublicationId> const& parent_ids) {
        if (exists(id)) {
            throw PublicationAlreadyCreated();
        }
        for (auto& parent_id: parent_ids) {
            if (!exists(parent_id)) {
                throw PublicationNotFound();
            }
        }
        Node* created_ptr = nullptr;
        std::vector<Node*> notified_parents;
        try {
            std::shared_ptr<Node> created = std::make_shared<Node>(id, map.get());
            created_ptr = created.get();
            std::tie(created->iterator, std::ignore) = map->emplace(id, created.get());
            for (auto& parent_id: parent_ids) {
                Node* parent = map->at(parent_id);
                created->parents.emplace(parent);
                parent->children.insert(created);
                notified_parents.push_back(parent);
            }
        } catch (...) {
            if (created_ptr != nullptr) {
                for (Node* parent: notified_parents) {
                    parent->children.erase(created_ptr->shared_from_this());
                }
            }
            throw;
        }
    }

    // Dodaje nową krawędź w grafie cytowań. Zgłasza wyjątek PublicationNotFound,
    // jeśli któraś z podanych publikacji nie istnieje.
    void add_citation(PublicationId const& child_id, PublicationId const& parent_id) {
        if (!exists(child_id) || !exists(parent_id)) {
            throw PublicationNotFound();
        }
        Node* child = map->at(child_id);
        Node* parent = map->at(parent_id);
        bool parent_added = false;
        try {
            child->parents.insert(parent);
            parent_added = true;
            parent->children.insert(child->shared_from_this());
        } catch (...) {
            if (parent_added) {
                child->parents.erase(parent);
            }
            throw;
        }
    }

    // Usuwa publikację o podanym identyfikatorze. Zgłasza wyjątek
    // PublicationNotFound, jeśli żądana publikacja nie istnieje. Zgłasza wyjątek
    // TriedToRemoveRoot przy próbie usunięcia pierwotnej publikacji.
    void remove(PublicationId const& id) {
        if (root->publication.get_id() == id) {
            throw TriedToRemoveRoot();
        }
        if (!exists(id)) {
            throw PublicationNotFound();
        }
        Node* me = map->at(id);
        std::set<Node*> parents = me->parents;
        for (Node* parent_ptr: parents) {
            parent_ptr->children.erase(me->shared_from_this());
        }
    }
};

#endif //JNP_CITATION_GRAPH_CITATIONGRAPH_H
