#include "citation_graph.h"

#include <cassert>
#include <exception>
#include <iostream>
#include <string>
#include <vector>

class Publication {
public:
    typedef typename std::string id_type;

    Publication(id_type const& _id) : id(_id) {
    }

    id_type get_id() const noexcept {
        return id;
    }

private:
    id_type id;
};

int main() {
    CitationGraph<Publication> gen("0");
    gen.create("A", "0");
//    gen.create("B", "A");
//    gen.create("C", "A");
//    gen.create("D", "B");
//    gen.add_citation("D", "A");
    gen.remove("A");
    return 0;
}