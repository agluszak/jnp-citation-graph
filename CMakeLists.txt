cmake_minimum_required(VERSION 3.12)
project(citation-graph)

set(Boost_USE_STATIC_LIBS ON)
find_package(Boost REQUIRED COMPONENTS unit_test_framework)
include_directories(${Boost_INCLUDE_DIR})

set(CMAKE_CXX_STANDARD 17)

add_executable(citation-graph citation_graph_example.cc)
add_executable(unit-tests citation_graph_unit_tests.cc)
add_executable(simple citation_graph_simple.cc)
target_link_libraries(unit-tests ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY})